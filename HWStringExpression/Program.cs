﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace HWStringExpression
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            ParseHTML(@"https://www.google.com/");

            Console.ReadKey();
        }

        private static void ParseHTML(string url)
        {
            var web = new WebClient();
            var html = web.DownloadString(url);

            var pattern = @"['""\s*<>]?(?<a>(https?|ftp|rtmp)://[^\s'""><]+)['""\s*><]?";

            var urls = Regex.Match(html, pattern);

            StringBuilder sb = new StringBuilder();
            while(urls.Success)
            {
                sb.AppendLine(urls.Groups["a"].ToString());
                urls = urls.NextMatch();
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
